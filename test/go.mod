module registry.gitlab.com/docker51/molecule

go 1.14

require (
	github.com/gruntwork-io/terratest v0.26.0
	github.com/stretchr/testify v1.5.1
)
