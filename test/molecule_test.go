package test

import (
	"fmt"
	"os"
	"os/exec"
	"testing"

	"github.com/gruntwork-io/terratest/modules/docker"
	"github.com/stretchr/testify/assert"
)

func verifyPaths(t *testing.T, tag string, tool string) {
	opts := &docker.RunOptions{
		Entrypoint: "which",
		Command:    []string{tool},
	}
	output := docker.Run(t, tag, opts)
	assert.Contains(t, output, "/home/molecule/.local/bin/"+tool)
}

func TestMolecule(t *testing.T) {

	registry_image := os.Getenv("CI_REGISTRY_IMAGE")
	tag := ""

	if registry_image == "" {
		tag = "registry.gitlab.com/docker51/molecule" + ":latest"
	} else {
		tag = registry_image + ":latest"
	}

	cmd := exec.Command("docker", "build", "--cache-from", tag, "--tag", tag, "../")
	stdout, err := cmd.Output()

	if err != nil {
		fmt.Println(err.Error())
		return
	} else {
		fmt.Printf("Container built!\n")
		fmt.Printf("%s\n", stdout)
	}

	opts := &docker.RunOptions{}
	output := docker.Run(t, tag, opts)
	assert.Contains(t, output, "molecule")
	assert.Contains(t, output, "ansible")
	assert.Contains(t, output, "python")

	opts = &docker.RunOptions{
		Entrypoint: "docker",
		Command:    []string{"--version"},
	}
	output = docker.Run(t, tag, opts)
	assert.Contains(t, output, "Docker version")

	opts = &docker.RunOptions{
		Entrypoint: "sh",
		Command:    []string{"-c", "echo $PATH"},
	}
	output = docker.Run(t, tag, opts)
	assert.Contains(t, output, "/home/molecule/.local/bin")

	tools := []string{"ansible", "ansible-playbook", "molecule"}
	for i, s := range tools {
		verifyPaths(t, tag, s)
		fmt.Printf("%d\n", i)
	}
}
