FROM docker:20.10.3
RUN apk --update add --no-cache \
    make=4.3-r0 \
    git=2.30.1-r0 \
    shadow=4.8.1-r0 \
    cargo=1.47.0-r2 \
    python3=3.8.7-r1 \
    libffi-dev=3.3-r2 \
    py3-pip=20.3.4-r0 \
    musl-dev=1.2.2-r0 \
    docker=20.10.3-r0 \
    gcc=10.2.1_pre1-r3 \
    python3-dev=3.8.7-r1 \
    openssl-dev=1.1.1j-r0 \
    linux-headers=5.7.8-r0 \
    && groupadd -g 1000 molecule \
    && useradd --no-log-init -m -u 1000 -g 1000 -G docker molecule
USER molecule
RUN python3 -m pip install --no-cache-dir --upgrade --user setuptools==53.0.0 \
    && python3 -m pip install --no-cache-dir --upgrade --user "molecule[docker]"==3.2.3 \
    && python3 -m pip install --no-cache-dir --upgrade --user "molecule[lint]==3.2.3"

ENV PATH "$PATH:/home/molecule/.local/bin"
ENTRYPOINT ["molecule"]
CMD ["--version"]
